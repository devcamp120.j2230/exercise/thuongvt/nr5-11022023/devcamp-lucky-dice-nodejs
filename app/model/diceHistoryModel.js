// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const DiceHistorySchema = new schema ({
    user:{type: mongoose.Types.ObjectId, ref:"User"},
    dice:{type: Number, require: true}
},{timestamps:true});

// exprot mongoose
module.exports = mongoose.model("DiceHistory",DiceHistorySchema);
