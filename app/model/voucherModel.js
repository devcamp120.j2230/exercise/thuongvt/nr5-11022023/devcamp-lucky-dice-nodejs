// khai báo thư viện mongoose
const mongoose = require("mongoose");
// khai báo class schema của mongo
const schema = mongoose.Schema;
//khai báo  review schema
const VoucherSchema = new schema({
    code: {type: String, unique: true, require: true},
    discount:{type:Number, require: true},
    note:{type: String, require:false}
},{timestamps:true});
// exprot mongoose
module.exports = mongoose.model("Voucher",VoucherSchema);
