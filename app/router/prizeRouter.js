//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const prizeMiddlerware = require("../middleware/middleware");
//Khai báo controller
const prizeController = require("../controller/prizeController");
//Tạo router
const prizeRouter = express.Router();
prizeRouter.post("/prizes",prizeMiddlerware.Middeware,prizeController.createPrize);
prizeRouter.get("/prizes",prizeMiddlerware.Middeware,prizeController.getAllPrize);
prizeRouter.get("/prizes/:prizeId",prizeMiddlerware.Middeware,prizeController.getPrizeById);
prizeRouter.put("/prizes/:prizeId",prizeMiddlerware.Middeware,prizeController.updatePrizeById);
prizeRouter.delete("/prizes/:prizeId",prizeMiddlerware.Middeware,prizeController.deletePrizeById);


// exprot router
module.exports = {prizeRouter}