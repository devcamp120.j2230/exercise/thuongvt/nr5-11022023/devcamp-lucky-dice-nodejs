//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const diceController = require("../controller/dice.controller");
//tạo router 
const diceRouter = express.Router();

diceRouter.post("/devcamp-lucky-dice/dice",middlerware.Middeware,diceController.diceHandler)
diceRouter.get("/devcamp-lucky-dice/dice-history",middlerware.Middeware,diceController.getDiceHistoryByUsername);
diceRouter.get("/devcamp-lucky-dice/voucher-history",middlerware.Middeware,diceController.getVoucherByUsername);
diceRouter.get("/devcamp-lucky-dice/prize-history",middlerware.Middeware,diceController.getPrizeHistory);
module.exports = {diceRouter}