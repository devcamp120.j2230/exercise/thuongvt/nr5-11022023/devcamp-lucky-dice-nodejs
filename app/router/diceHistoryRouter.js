//Khai báo một thư viện express
const express = require ('express');
// improt middlerware 
const middlerware = require("../middleware/middleware");
//Khai báo controller
const diceHistoryController = require("../controller/diceHistoryController");
//Tạo router 
const diceHistoryRouter = express.Router();

diceHistoryRouter.post("/dice-histories",middlerware.Middeware,diceHistoryController.createDiceHistory);
diceHistoryRouter.get("/dice-histories",middlerware.Middeware,diceHistoryController.getAllDiceHistory);
diceHistoryRouter.get("/dice-histories/:diceHistoryId",middlerware.Middeware,diceHistoryController.getDiceHistoryById);
diceHistoryRouter.put("/dice-histories/:diceHistoryId",middlerware.Middeware,diceHistoryController.updateDiceHistoryById);
diceHistoryRouter.delete("/dice-histories/:diceHistoryId",middlerware.Middeware,diceHistoryController.deleteDiceHistoryById)

// exprot router
module.exports = {diceHistoryRouter}